from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

# Specifying incognito mode as you launch your browser[OPTIONAL]
option = webdriver.ChromeOptions()
option.add_argument("--incognito")

# Create new Instance of Chrome in incognito mode
driver = webdriver.Chrome(chrome_options=option)
driver.get('http://booking.com')
driver.find_element_by_css_selector("#destination").send_keys("Berlin")
WebDriverWait(driver, 1, poll_frequency=0.1).\
    until(lambda drv: len(drv.find_elements_by_css_selector("ul.ui-autocomplete li")) > 0)
driver.find_element_by_css_selector("ul.ui-autocomplete li").click()
driver.find_element_by_css_selector("#availcheck").click()
driver.find_element_by_css_selector("#searchbox_btn").submit()
for link in driver.find_elements_by_css_selector("a.hotel_name_link"):
    print(link.text)