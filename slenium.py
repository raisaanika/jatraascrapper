import openpyxl
import xlwt
from openpyxl import Workbook
from selenium import webdriver
from selenium.webdriver.common.by import By
from getpass import getpass
import time
import codecs
import sys
import os

def writingToExcel(label, listItems, columNumber):
    sheet.cell(row=1, column=columNumber).value = label
    i = 2
    j = 0
    while j < (len(listItems)):
        sheet.cell(row=i, column=columNumber).value = listItems[j]
        i += 1
        j += 1

'''
UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)
'''

# Specifying incognito mode as you launch your browser[OPTIONAL]
option = webdriver.ChromeOptions()


# Create new Instance of Chrome in incognito mode
driver = webdriver.Chrome(chrome_options=option)

# driver = webdriver.ChromeOptions()
driver.get('https://www.booking.com/hotel/bd/civic-inn-dhaka1.html?label=gen173nr-1FCAEoggI46AdIM1gEaBSIAQGYATG4ARfIAQzYAQHoAQH4AQKIAgGoAgM;sid=27b587eacfc6b811c4504b535379352a;all_sr_blocks=276654111_106995423_0_41_0;checkin=2018-11-23;checkout=2018-11-25;dest_id=-2737683;dest_type=city;dist=0;group_adults=2;hapos=5;highlighted_blocks=276654111_106995423_0_41_0;hpos=5;room1=A%2CA;sb_price_type=total;srepoch=1542861804;srfid=6655e6e9676e1232df236c4a927e68e30167b759X5;srpvid=cd472135e0fb001f;type=total;ucfs=1&#hotelTmpl')  # facebook link

'''
place_input = driver.find_element_by_id('ss')
place_input.send_keys('Berlin');

submit_button = driver.find_element_by_xpath("//div[contains(@class,'sb-searchbox-submit-col -submit-button')]//button[@type='submit']")
submit_button.submit()
'''

#hotel_links = driver.find_element_by_xpath("//div[@id='hotellist_inner']//div[1]//div[2]//div[1]//div[1]//h3[1]//a[1]").click()

room_type = driver.find_elements_by_xpath("//span[contains(@class,'hprt-roomtype-icon-link')][text()]")
room_type_text = [x.text for x in room_type]


hotel_name = driver.find_elements_by_xpath("//h2[@id='hp_hotel_name'][text()]")
hotel_name_text = [x.text for x in hotel_name]


room_price = driver.find_elements_by_xpath("//div[contains(@class,'hprt-price-price')]//span[text()]")
room_price_text = [x.text for x in room_price]

'''
username_box = driver.find_element_by_id('email')
username_box.send_keys(usr)
'''

wb = Workbook()
filepath = "output.xlsx" #output file name
sheet = wb.active #open xl file


writingToExcel('Hotel Name', hotel_name_text, 1)
writingToExcel('Room Type', room_type_text, 2)
writingToExcel('Room Price', room_price_text, 3)


wb.save(filepath) #save xl file