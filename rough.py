import datetime 
import time

#x1 = datetime.datetime.now().strftime("%Y-%m-%d")
#x = datetime.datetime.fromtimestamp(int(time.time())).strftime("%Y-%m-%d")

#print (x)


check_in_date = datetime.datetime.fromtimestamp(int(time.time())).strftime("%Y-%m-%d")

##1 day
check_out_date = datetime.datetime.fromtimestamp(int(time.time()) + 86400).strftime("%Y-%m-%d")

##1 week
#check_out_date = datetime.datetime.fromtimestamp(int(time.time()) + 604800).strftime("%Y-%m-%d")

##1 month
#check_out_date = datetime.datetime.fromtimestamp(int(time.time()) + 2592000).strftime("%Y-%m-%d")



### function to select date
def select_date(date_to_select, isCheckInDate=True):
    data_mode = "checkin" if isCheckInDate else "checkout"

    # Wait datepicker visible
    try:
        locator = 'div[data-mode="{0}"] + div'.format(data_mode)
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, locator)))

    except TimeoutException:
        # click the `+` button to display the datepicker
        locator = 'div[data-mode="{0}"] button.sb-date-field__icon-btn'.format(data_mode)

        driver.find_element_by_css_selector(locator).click()

    dateFound = False

    # iterate 12 months
    for i in rang(0, 12):

        # the datepicker which will display two month per time
        # find the first visible month in datepicker

        locator = 'div[data_mode="{0}"] + div div.c2-calendar-inner'.format(data_mode)
        style = driver.find_element_by_css_selector(locator).get_attribute('style')

        margin_left = re.search(r'(\d+)', style).group(1)

        # find all days in first visible month indatepicker
        locator = 'div[data_mode="{0}"] + div div[data-offset="{1}"]' + ' td[data-id]:not(.c2-day-s-disabled)'.format(data_mode, margin_left)

        days = driver.find_elements_by_css_selector(locator)

        for day in days:
            data_id = day.get_attribute("data-id")
            date = datetime.fromtimestamp(int(data_id[:-3])).strftime("%Y-%m-%d")
            print ('iterate date: ' + date)

            if date == date_to_select:
                day.click()
                dateFound = True
                return

        # if not found in first visible month,
        # click the right arrow to scroll month 
        locator = 'div[data_mode="{0}"] + div  div.c2-button-further'.format(data_mode)
        driver.find_element_by_css_selector(locator).click()

    if dateFound == False:
        print ('Not found {0} date: {1}'.format(data_mode, date_to_select))

# choose check in date
x1 = select_date(check_in_date)

# choose check out date
x2 = select_date(check_out_date, False)


print (x1)
print (x2)

print (check_in_date)
print (check_out_date)