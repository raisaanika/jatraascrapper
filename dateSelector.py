import openpyxl
import xlwt
from openpyxl import Workbook
from selenium import webdriver
from selenium.webdriver.common.by import By
from getpass import getpass
import time
import codecs
import sys
import os

def writingToExcel(label, listItems, columNumber):
    sheet.cell(row=1, column=columNumber).value = label
    i = 2
    j = 0
    while j < (len(listItems)):
        sheet.cell(row=i, column=columNumber).value = listItems[j]
        i += 1
        j += 1

'''
UTF8Writer = codecs.getwriter('utf8')
sys.stdout = UTF8Writer(sys.stdout)
'''

# Specifying incognito mode as you launch your browser[OPTIONAL]
option = webdriver.ChromeOptions()


# Create new Instance of Chrome in incognito mode
driver = webdriver.Chrome(chrome_options=option)

# driver = webdriver.ChromeOptions()
driver.get('https://www.booking.com/')  # facebook link

place_input = driver.find_element_by_id('ss')
place_input.send_keys('Dhaka');

date_opener = driver.find_element_by_xpath("//div[@class='xp__dates-inner xp__dates__checkin']//button[@type='button']")
date_opener.click()


date_check_in = driver.find_element_by_xpath("//td[@data-date='2018-11-28'][text()]")
date_check_in.click()

date_check_out = driver.find_element_by_xpath("//td[@data-date='2018-11-30'][text()]")
date_check_out.click()


submit_button = driver.find_element_by_xpath("//div[contains(@class,'sb-searchbox-submit-col -submit-button')]//button[@type='submit']")
submit_button.submit()