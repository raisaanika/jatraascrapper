import requests
import re
from bs4 import BeautifulSoup
from bs4.element import Tag
import openpyxl
import xlwt
from openpyxl import Workbook

from getpass import getpass
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
#from datetime import datetime
import time
import datetime
import codecs
import sys
import os


### function to select date
def select_date(date_to_select, isCheckInDate=True):
    data_mode = "checkin" if isCheckInDate else "checkout"

    # Wait datepicker visible
    try:
        locator = 'div[data-mode="{0}"] + div'.format(data_mode)
        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, locator)))

    except TimeoutException:
        # click the `+` button to display the datepicker
        locator = 'div[data-mode="{0}"] button.sb-date-field__icon-btn'.format(data_mode)

        driver.find_element_by_css_selector(locator).click()

    dateFound = False

    # iterate 12 months
    for i in range(0, 12):

        # the datepicker which will display two month per time
        # find the first visible month in datepicker

        locator = 'div[data_mode="{0}"] + div div. '.format(data_mode)
        style = driver.find_element_by_css_selector(locator).get_attribute('style')

        margin_left = re.search(r'(\d+)', style).group(1)

        # find all days in first visible month indatepicker
        locator = 'div[data_mode="{0}"] + div div[data-offset="{1}"]' + ' td[data-id]:not(.c2-day-s-disabled)'.format(data_mode, margin_left)

        days = driver.find_elements_by_css_selector(locator)

        for day in days:
            data_id = day.get_attribute("data-id")
            date = datetime.fromtimestamp(int(data_id[:-3])).strftime("%Y-%m-%d")
            print ('iterate date: ' + date)

            if date == date_to_select:
                day.click()
                dateFound = True
                return

        # if not found in first visible month,
        # click the right arrow to scroll month 
        locator = 'div[data_mode="{0}"] + div  div.c2-button-further'.format(data_mode)
        driver.find_element_by_css_selector(locator).click()

    if dateFound == False:
        print ('Not found {0} date: {1}'.format(data_mode, date_to_select))
#UTF8Writer = codecs.getwriter('utf8')
#sys.stdout = UTF8Writer(sys.stdout)

# usr = input('Enter your username or email id: ')#development purpose
# pwd = getpass('enter your password: ')#development purpose
#usr = "jahidnj@ymail.com"  # testing purpose
#pwd = "A1b2c3d4e5"  # testing purpose

# Specifying incognito mode as you launch your browser[OPTIONAL]
option = webdriver.ChromeOptions()
option.add_argument("--incognito")

# Create new Instance of Chrome in incognito mode
driver = webdriver.Chrome(chrome_options = option)

# driver = webdriver.ChromeOptions()
driver.get('https://www.booking.com/')  


#username_box = driver.find_element_by_id('email')
#username_box.send_keys(usr)

##Manipulate Calender
date_now = datetime.datetime.now()
print (date_now)

#calender_select = driver.find_element_by_id('frm')
#calender_select = driver.find_element_by_xpath ("/html[1]/body[1]/div[4]/div[1]/div[1]/div[2]/form[1]/div[1]/div[2]/div[1]/div[3]/div[1]/div[1]/div[1]/div[1]/button[1]")
#calender_select.submit()

##Taking date range
check_in_date = datetime.datetime.fromtimestamp(int(time.time())).strftime("%Y-%m-%d")

##1 day
check_out_date = datetime.datetime.fromtimestamp(int(time.time()) + 86400).strftime("%Y-%m-%d")

##1 week
#check_out_date = datetime.datetime.fromtimestamp(int(time.time()) + 604800).strftime("%Y-%m-%d")

##1 month
#check_out_date = datetime.datetime.fromtimestamp(int(time.time()) + 2592000).strftime("%Y-%m-%d")



# choose check in date
select_date(check_in_date)

# choose check out date
select_date(check_out_date, False)


##Location Search
search_box = driver.find_element_by_id('ss')
search_box.send_keys('Berlin');

submit_button = driver.find_element_by_xpath("//div[contains(@class,'sb-searchbox-submit-col -submit-button')]//button[@type='submit']")
submit_button.submit();


#driver.get('https://www.facebook.com/groups/cookupsBD/')  # testing purpose
#ls
#driver.get('https://www.booking.com/searchresults.en-gb.html?label=gen173nr-1DCAEoggI46AdIM1gEaBSIAQGYAQm4AQfIAQzYAQPoAQGIAgGoAgM&sid=5467464c05d91d15a2ff00a85e592bfd&sb=1&src=index&src_elem=sb&error_url=https%3A%2F%2Fwww.booking.com%2Findex.en-gb.html%3Flabel%3Dgen173nr-1DCAEoggI46AdIM1gEaBSIAQGYAQm4AQfIAQzYAQPoAQGIAgGoAgM%3Bsid%3D5467464c05d91d15a2ff00a85e592bfd%3Bsb_price_type%3Dtotal%26%3B&ss=Dhaka%2C+Bangladesh&ssne=DHANMONDI&ssne_untouched=DHANMONDI&checkin_monthday=14&checkin_month=11&checkin_year=2018&checkout_monthday=15&checkout_month=11&checkout_year=2018&no_rooms=1&group_adults=2&group_children=0&b_h4u_keep_filters=&from_sf=1&ss_raw=DHAKA&ac_position=0&ac_langcode=en&ac_click_type=b&dest_id=-2737683&dest_type=city&iata=DAC&place_id_lat=23.723101&place_id_lon=90.4086&search_pageview_id=83052e2a4a6f04d5&search_selected=true')  # testing purpose
time.sleep(10);

wb = Workbook()
filepath = "demo.xlsx"
sheet = wb.active

hotel_name = driver.find_element_by_xpath("/html[1]/body[1]/div[5]/div[1]/div[3]/div[1]/div[1]/div[5]/div[3]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/h3[1]/a[1]/span[1]")
hotel = [x.text for x in hotel_name]
print (hotel)
#coock_name_junk = driver.find_elements_by_xpath("//span[@class='fwn fcg']//a[@class='profileLink'][text()]")
#coock = [x.text for x in coock_name_junk]  # coock name
#cooke = coock


data = [('Hotel'),
        (hotel)]

for row in data:
    sheet.append(row)
wb.save(filepath)

#datepicker_to=driver.find_element_by_xpath("//input[@id='to']")
#calender 
#pick date from calender and show
